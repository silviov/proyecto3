#include <stdio.h>
#include <stdlib.h>
#include <string.h>
typedef struct{//declaracion de estructura
	char nombre[84];//seccion de declaracion de variables
	float codigo;
	float nem;
	float ranking;
	float mate;
	float leng;
	float historia;
	float ciencia;
	float pond;
	float psu;
	float max;
	float min;
	float bacantespsu;
	float bacantesbea;
} univ;

void cargarFacultades(char path[], univ carrera[]){//declaracion de la funcion para mostrar facultades
	FILE *file;//declaracion de variables
	int i = 0;
	if((file=fopen(path,"r"))==NULL){//condicion para ejecutar una accion en caso de que no se encuentre el archivo
		printf("\nerror al abrir el archivo");
		exit(0); 
	}
	else{//condicion contraria a la anterior 
		printf("\nLas ponderaciones son: \n");
		printf("carrera		Codigo	Nem	Rank	Leng	Mat	Hist	Cs	\n");
		while (feof(file) == 0) {//lee, almazena e imprime nuestra lista separandola por tabulaciones
				fscanf(file,"%s %f %f %f %f %f %f %f %f %f %f %f %f %f \n",carrera[i].nombre,&carrera[i].codigo,&carrera[i].nem,&carrera[i].ranking,&carrera[i].mate,&carrera[i].leng,&carrera[i].historia,&carrera[i].ciencia,&carrera[i].pond,&carrera[i].psu,&carrera[i].max,&carrera[i].min,&carrera[i].bacantespsu,&carrera[i].bacantesbea);
				printf("%s\t%.0f\t%.0f\t%.0f\t%.0f\t%.0f\t%.0f\t%.0f\t\n", carrera[i].nombre,carrera[i].codigo, carrera[i].nem,carrera[i].ranking,carrera[i].mate,carrera[i].leng,carrera[i].historia,carrera[i].ciencia);
				i++;
			}

		fclose(file);//cierra el archivo leido

	}
}
void buscarnom(char path[],univ carrera[]){//funcion para buscar carrera en base al nombre o codigo
	FILE *file;//declaracion de variables
	char name[90];
	int i,j,var1,var2;
	if((file=fopen(path,"r"))==NULL){//condicion para ejecutar una accion en caso de que no se encuentre el archivo
		printf("\nerror al abrir el archivo");
		exit(0); 
	}
	else{//condicion contraria a la anterior
		while (feof(file) == 0) {//lee, almazena e imprime nuestra lista separandola por tabulaciones
				fscanf(file,"%s %f %f %f %f %f %f %f %f %f %f %f %f %f \n",carrera[i].nombre,&carrera[i].codigo,&carrera[i].nem,&carrera[i].ranking,&carrera[i].mate,&carrera[i].leng,&carrera[i].historia,&carrera[i].ciencia,&carrera[i].pond,&carrera[i].psu,&carrera[i].max,&carrera[i].min,&carrera[i].bacantespsu,&carrera[i].bacantesbea);
				i++;
		}
		printf("Ingrese el nombre de la carrera:\n");
		scanf("%s",name);
		for (int i = 0; i < 51; i++){//para ir recorriendo el arreglo total de las carreras y luego poder compararlas con el strcmp con la plabra pedida por el usuario
			var1=strcmp(carrera[i].nombre,name);
			}if(var1==0){//si var1 es igual a 0 lanza un mensaje de que encontro la coincidencia
				printf("iguales\n");
				var2=i;}//toma el valor de donde encontro la coincidencia y se la asigna a la variable
			else{//caso contrario muestra un mensaje de que no encontro coincidencia
				printf("son diferentes\n");
				}
		
		fclose(file);//se cierra el archivo

		
		
	}
}

/*void buscarcod(char path[],univ carrera[]){//funcion para buscar carrera en base al nombre o codigo
	FILE *file;//declaracion de variables
	char code[90];
	int i,j,var1,var2;
	if((file=fopen(path,"r"))==NULL){//condicion para ejecutar una accion en caso de que no se encuentre el archivo
		printf("\nerror al abrir el archivo");
		exit(0); 
	}
	else{//condicion contraria a la anterior
		while (feof(file) == 0) {//lee, almazena e imprime nuestra lista separandola por tabulaciones
				fscanf(file,"%s %f %f %f %f %f %f %f %f %f %f %f %f %f \n",carrera[i].nombre,&carrera[i].codigo,&carrera[i].nem,&carrera[i].ranking,&carrera[i].mate,&carrera[i].leng,&carrera[i].historia,&carrera[i].ciencia,&carrera[i].pond,&carrera[i].psu,&carrera[i].max,&carrera[i].min,&carrera[i].bacantespsu,&carrera[i].bacantesbea);
				i++;
		}
		printf("Ingrese el codigo de la carrera:\n");
		scanf("%f",&code);
		for (int i = 0; i < 51; i++){//para ir recorriendo el arreglo total de las carreras y luego poder compararlas con el strcmp con la plabra pedida por el usuario
			//var1=strcmp(carrera[i].codigo,code);
			}if(var1==0){//si var1 es igual a 0 lanza un mensaje de que encontro la coincidencia
				printf("iguales\n");
				var2=i;}//toma el valor de donde encontro la coincidencia y se la asigna a la variable
			else{//caso contrario muestra un mensaje de que no encontro coincidencia
				printf("son diferentes\n");
				}
		
		fclose(file);//se cierra el archivo

		
		
	}
}
*/ //se dejo esta variable comentada por que generaba errorers que no pudimos determinar
void menu(univ carrera[]){//funcion del menu
	int opcion,opcion2,opcion3;//declaracion de variables
	do{
		printf( "\n   1. Consultar Ponderación Carrera" );//se declaran las condiciones del menu
		printf( "\n   2. Simular Postulación Carrera" );
		printf( "\n   3. Mostrar Ponderaciones Facultad" );
		printf( "\n   4. Salir." );
		printf( "\n\n   Introduzca opcion (1-4): " );
		scanf( "%d", &opcion );
		switch ( opcion ){
			case 1:
				do{
				printf("\n Seleccione su metodo de busqueda");
				printf("\n1.- Buscar por nombre");
				printf("\n2.- Buscar por codigo");
				printf( "\n\n   Introduzca opcion (1-2): " );
				scanf("%d",&opcion2);
					switch(opcion2){
						case 1:
						printf("\n1.- ingrese el nombre:\n ");
						buscarnom("todo.txt", carrera);//se llama a la funcion buscarnom
						break;
						case 2:
						printf("\n1.- ingrese el codigo:\n");
						//buscarcod("todo.txt", carrera);//se llama a la funcion buscarcod
						break;
					}
				break;}
				while(opcion!=2);
			case 2:  
					
					break;

			case 3: 
			do{
				printf("\n Seleccione La Facultad donde se encuentra su carrera: ");
				printf("\n1.- FACULTAD DE ARQUITECTURA");
				printf("\n2.- FACULTAD DE CIENCIAS");
				printf("\n3.- FACULTAD DE CIENCIAS DEL MAR Y DE RECURSOS NATURALES");
				printf("\n4.- FACULTAD DE CIENCIAS ECONÓMICAS Y ADMINISTRATIVAS");
				printf("\n5.- FACULTAD DE DERECHO Y CIENCIAS SOCIALES");
				printf("\n6.- FACULTAD DE FARMACIA");
				printf("\n7.- FACULTAD DE HUMANIDADES");
				printf("\n8.- FACULTAD DE INGENIERÍA");
				printf("\n9.- FACULTAD DE MEDICINA");
				printf("\n10.- FACULTAD DE ODONTOLOGÍA");
				printf( "\n\n   Introduzca opcion (1-10): " );
				scanf("%d", &opcion3);
				switch (opcion3){
					case 1:cargarFacultades("1.txt", carrera);//llama a la funcion cargarfacultades para trabajar los archivos txt
						break;
					case 2:cargarFacultades("2.txt", carrera);
						break;
					case 3:cargarFacultades("3.txt", carrera);
						break;
					case 4:cargarFacultades("4.txt", carrera);
						break;
					case 5:cargarFacultades("5.txt", carrera);
						break;
					case 6:cargarFacultades("6.txt", carrera);
						break;
					case 7:cargarFacultades("7.txt", carrera);
						break;
					case 8:cargarFacultades("8.txt", carrera);
						break;
					case 9:cargarFacultades("9.txt", carrera);
						break;
					case 10:cargarFacultades("10.txt", carrera);
						break;
				
				}
				break;
				}while(opcion!=10);

		
}
}while ( opcion != 4 );

}
int main(){
	
	univ carrera[512]; //declaracion de la lista
	menu(carrera); 
	return EXIT_SUCCESS; 
}
